<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Service;
use App\Portfolio;
use App\People;
use Illuminate\Support\Facades\DB;
use Mail;

class IndexController extends Controller
{
    public function execute( Request $request){
		
		if($request->isMethod('post')) {
		
			$messages = [
			
			'required' => "The :attribute field must be filled out",
			'email' => "The field :attribute must be email" 
			
			];
			
			$this->validate($request, [
				'name' => 'required | max:255',
				'email' => 'required | email',
				'text' => 'required'
			
			], $messages );
			
			$data = $request->all();
			
			Mail::send('site.email', ['data' =>$data], function($message) use ($data){
				
				$mail_admin = 'adrubinov@gmail.com';
				$message->from($data['email'], $data['name']);
				$message->to($mail_admin)->subject('Question');
				
			});
			
			
		
		}
		
		$pages = Page::all();
		$portfolios = Portfolio::get(['name','filter', 'images']);
		$services = Service::where('id', '<', 20)->get();
		$peoples = People::take(3)->get();
		
		$tags = DB::table('portfolios')->distinct()->pluck('filter');
		
		
		$menu = [];
		foreach($pages as $page) {
			$item = ['title' => $page->name, 'alias' => $page->alias];
			array_push($menu, $item);
		}
		
		$item = ['title' => 'Service', 'alias' => 'service'];
		array_push($menu, $item);
			
		$item = ['title' => 'Portfolio', 'alias' => 'Portfolio'];
		array_push($menu, $item);
		
		$item = ['title' => 'Team', 'alias' => 'team'];
		array_push($menu, $item);
		
		$item = ['title' => 'Contact', 'alias' => 'contact'];
		array_push($menu, $item);
		
		return view('site.index', ['menu' =>$menu,
									'pages'=>$pages,
									'services'=> $services,
									'portfolios' =>$portfolios,
									'peoples' => $peoples, 
									'tags' => $tags,
		
									]);
		
	}
}
