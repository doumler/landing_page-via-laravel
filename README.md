Landing page
=============================

Landing page made with Laravel


Installation
------------

1. Import content of file land.sql in your database
2. Download files in your blog: git clone https://doumler@bitbucket.org/doumler/landing_page-via-laravel.git
3. Set access to the database in file .env of framework Laravel
4. To administrate your landing page input in the browser path: 'http://<your blog>/admin'.

End
------------