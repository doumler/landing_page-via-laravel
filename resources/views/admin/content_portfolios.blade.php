<div style="margin:0px 50px 0px 50px;">   

@if($portfolios)
 
	<table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>№</th>
                <th>Title</th>
                <th>Filter</th>
                <th>Images</th>
                <th>Created</th>
                
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($portfolios as $k => $portfolio)
        
        	<tr>
        	
        		<td>{{ $portfolio->id }}</td>
        		<td>{!! Html::link(route('portfolioEdit',['portfolio'=>$portfolio->id]),$portfolio->name,['alt'=>$portfolio->name]) !!}</td>
        		<td>{{ $portfolio->filter }}</td>
        		<td>{{$portfolio->images }}</td>
        		<td>{{ $portfolio->created_at }}</td>
        		
        		<td>
	        		{!! Form::open(['url'=>route('portfolioEdit',['portfolio'=>$portfolio->id]), 'class'=>'form-horizontal','method' => 'POST']) !!}
	        			
	        			{{ method_field('DELETE') }}
	        			{!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}
	        			
	        		{!! Form::close() !!}
        		</td>
        	</tr>
        
        @endforeach
        
		
        </tbody>
    </table>
@endif 

{!! Html::link(route('portfolioAdd'),'New portfolio') !!}
   
</div>